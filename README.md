ALAGUE ADMIN (ADMIN TEMPLATE)
========================================================================

> IN DEVELOPMENT. Don't use this template in your serious project.

## Wanna Try?

This template using blade template engine, so there is some requirements:

- PHP >= 5.4
- Composer for install PHP package dependencies
- Bower for install asset dependencies

If you have install those requirements, you need to: 

- clone this repository
- In terminal/cmd, goto to cloned directory
- run `composer install` for install framework dependencies
- run `bower install` for install assets requirements
- then run `php -S localhost:3000`