<div class="box-small rounded shadowed {{ $classes }}">
	<div class="icon {{ isset($icon_classes)? $icon_classes : '' }}">
		<i class="{{ $icon }}"></i>
	</div>
	<div class="info {{ isset($info_classes)? $info_classes : '' }}">
		<h4 class="value">{{ $value }}</h4>
		<hr class="dashed">
		<em class="title">{{ $label }}</em>
	</div>
</div>