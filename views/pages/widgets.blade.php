@extends('master')

@section('content')


	{{-- SMALL BOX WITH CIRCLE ICON --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon bg-blue',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon bg-green',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon bg-red',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon bg-purple',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
	</div>

	

	{{-- SMALL calendarX --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-blue',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-green',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-red',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-purple',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
			]) 
		!!}
		</div>
	</div>

	{{-- SMALL calendarX --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-blue',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
				'info_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-green',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
				'info_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-red',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
				'info_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-purple',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
				'info_classes' => 'bg-white',
			]) 
		!!}
		</div>
	</div>

	{{-- SMALL BOX --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-blue circle-icon',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
				'info_classes' => 'bg-white',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-green circle-icon',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
				'info_classes' => 'bg-white',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-red circle-icon',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
				'info_classes' => 'bg-white',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-purple circle-icon',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
				'info_classes' => 'bg-white',
				'icon_classes' => 'bg-white',
			]) 
		!!}
		</div>
	</div>

	{{-- SMALL BOX --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
				'icon_classes' => 'bg-blue',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
				'icon_classes' => 'bg-green',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
				'info_classes' => 'bg-white',
				'icon_classes' => 'bg-red',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
				'info_classes' => 'bg-white',
				'icon_classes' => 'bg-purple',
			]) 
		!!}
		</div>
	</div>


	{{-- SMALL BOX --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
				'icon_classes' => 'fg-blue',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
				'icon_classes' => 'fg-green',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
				'info_classes' => 'bg-white',
				'icon_classes' => 'fg-red',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'bg-white circle-icon',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
				'info_classes' => 'bg-white',
				'icon_classes' => 'fg-purple',
			]) 
		!!}
		</div>
	</div>


	{{-- SMALL BOX --}}
	<div class="row">
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon',
				'icon' => 'fa fa-users',
				'value' => 1428,
				'label' => 'Pendaftar baru',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon',
				'icon' => 'fa fa-list',
				'value' => 876,
				'label' => 'Kuota tersisa',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon',
				'icon' => 'fa fa-home',
				'value' => 234,
				'label' => 'Kelas',
			]) 
		!!}
		</div>
		<div class="col-md-3 col-sm-6">
		{!! 
			$app->view->render('partials.small-box', [
				'classes' => 'circle-icon',
				'icon' => 'fa fa-calendar',
				'value' => '21 hari',
				'label' => 'Batas pendaftaran',
			]) 
		!!}
		</div>
	</div>


@stop