<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ $app->asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}"/>
        <link rel="stylesheet" href="{{ $app->asset('bower_components/font-awesome/css/font-awesome.min.css') }}"/>
        <link rel="stylesheet" href="{{ $app->asset('dist/css/alague-admin.css') }}"/>
    </head>
    <body class="app-view">
        <header>
            <div class="left-side">
                <a href="" class="brand">Brand</a>
            </div>
            <div class="right-side">
                @include('partials.nav-header')
            </div>
        </header>
        <aside>
            <div class="sidebar">
            	Sidebar
                @include('partials.nav-sidebar')
            </div>
        </aside>
        <div id="content-wrapper">
            <div class="content">
            @yield('content')
            </div>
        </div>
        <footer>
            @include('partials.footer')
        </footer>

        @section('scripts')
        <script src="{{ $app->asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ $app->asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        @stop
    </body>
</html>