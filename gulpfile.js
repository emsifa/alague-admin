var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var minifyCSS = require('gulp-minify-css');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var output_path = {
	css: 'dist/css',
	js: 'dist/js'
};

function plumberError(title, message) {
	return plumber({
		errorHandler: notify.onError({
			message: '<%= error.message %>',
			title: title
		})
	});
}

function notifySuccess(title, message) {
	return notify({
		message: message,
		title: title
	});
}

gulp.task('less', function() {
	gulp.src('less/alague-admin.less')
		.pipe(plumberError('Compile Less'))
		.pipe(less())
		.pipe(gulp.dest(output_path.css))
		.pipe(notifySuccess('Compile Less', '<%= file.path %> generated!'))
});

gulp.task('minify-css', function() {
	gulp.src(output_path.css+'/alague-admin.css')
		.pipe(plumberError('Minify CSS'))
		.pipe(minifyCSS())
		.pipe(rename({
			basename: 'alague-admin.min'
		}))
		.pipe(gulp.dest(output_path.css))
		.pipe(notifySuccess('Minify CSS', '<%= file.path %> generated!'))
});

gulp.task('watch', function() {
    gulp.watch('**/*.less', ['less', 'minify-css']);
});

gulp.task('default', ['watch', 'less', 'minify-css']);