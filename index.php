<?php

require("vendor/autoload.php");

use Rakit\Framework\App;
use Rakit\Blade\RakitProvider as BladeProvider;

/**
  * #1 Setup Rakit App
  * -------------------------------------------------------------------
  * In Demo, just Instantiate rakit application,
  * add a macro for correctly call asset url  
  * and register BladeProvider for blade template engine
  */ 
$app = new App();
$app->config['view.path'] = __DIR__.'/views';
$app->config['view.cache_path'] = __DIR__.'/view-caches';
$app->register(new BladeProvider);

$app->macro('asset', function($file) {
	$base_url = "http://localhost:3000"; // << you may change this

	return $base_url.'/'.$file;
});

/**
 * #2 register app routes
 * -------------------------------------------------------------------
 * we only need single route for handling demo pages
 * if you are looking for page files, look at 'view/pages' directory
 */
$app->get('/:page?')->action(function($req, $res) use ($app) {
	$page = $req->param('page', 'index');
	$view_file = "pages.{$page}";

	// if page (blade) file not exists, return 404
	if(!$app->blade->exists($view_file)) {
		return $res->send("{$view_file} not found", 404);
	}

	$app->view = $res->view;
	return $res->view($view_file, ['app' => $app]);
});

/**
 * #3 Run App
 * ---------------------------------------------------------------------
 * now you can run PHP built-in server via command:
 * > php -S localhost:3000
 */
$app->run();